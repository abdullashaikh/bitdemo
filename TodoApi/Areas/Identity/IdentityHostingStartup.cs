﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(TodoApi.Areas.Identity.IdentityHostingStartup))]
namespace TodoApi.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {


            });
        }
    }
}
