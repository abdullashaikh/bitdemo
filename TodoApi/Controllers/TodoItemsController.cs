﻿/*
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using TodoApi.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Security.Claims;

using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using TodoApi.Authentication;

namespace BookServices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationController : ControllerBase
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IConfiguration Configuration;

        public ApplicationController(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            Configuration = configuration;

        }

        //Register userRole
        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            var userExit = await userManager.FindByNameAsync(model.UserName);
            if (userExit != null)
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", massage = "User Already Exits" });
            ApplicationUsers user = new ApplicationUsers()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.UserName

            };
            var result = await userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", massage = "Account Creation Failed" });
            }

            return Ok(new Response { status = "Success", massage = "Account create successfully" });

        }

        //Register AdminRole
        [HttpPost]
        [Route("RegisterAdmin")]
        public async Task<IActionResult> RegisterAdmin([FromBody] RegisterModel model)
        {
            var userExit = await userManager.FindByNameAsync(model.UserName);
            if (userExit != null)
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", massage = "User Already Exits" });
            ApplicationUsers user = new ApplicationUsers()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.UserName

            };
            var result = await userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", massage = "Account Creation Failed" });
            }
            if (!await roleManager.RoleExistsAsync(userRoles.Admin))
                await roleManager.CreateAsync(new IdentityRole(userRoles.Admin));
            if (!await roleManager.RoleExistsAsync(userRoles.User))
                await roleManager.CreateAsync(new IdentityRole(userRoles.User));
            if (!await roleManager.RoleExistsAsync(userRoles.Admin))
            {
                await userManager.AddToRoleAsync(user, userRoles.Admin);
            }
            return Ok(new Response { status = "Success", massage = "Account create successfully" });

        }

        //private readonly IJwtAuth jwtAuth;
        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            var user = await userManager.FindByNameAsync(model.UserName);
            if (user != null && await userManager.CheckPasswordAsync(user, model.Password))
            {
                var userRoles = await userManager.GetRolesAsync(user);
                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name,user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),

                };
                foreach (var userRole in userRoles)
                {

                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                }
                var authSinginKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]));
                var token = new JwtSecurityToken(

                    issuer: Configuration["JWT:ValidIssuer"],
                    audience: Configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSinginKey, SecurityAlgorithms.HmacSha256));

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token)

                });
            }
            return Unauthorized();
        }


  //----------------------------------------------------------------------------------------->>>>>

        [HttpGet]     
        [Route("UserInfo")]
        public async Task<IActionResult> UserInfo([FromBody] UserProfile model)
        {
           var userExit = await userManager.FindByNameAsync(model.UserName);
            if (userExit != null)
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", massage = "User Already Exits" });
            ApplicationUsers user = new ApplicationUsers()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.UserName

            };
          if (!await roleManager.RoleExistsAsync(UserProfile.UserPhone))
                await roleManager.CreateAsync(new IdentityRole(UserProfile.UserPhone));
          if (!await roleManager.RoleExistsAsync(UserProfile.UserName))
                await roleManager.CreateAsync(new IdentityRole(UserProfile.UserName));


           return OK(model.userName);
        }

    }
}


*/