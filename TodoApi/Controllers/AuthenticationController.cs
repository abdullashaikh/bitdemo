﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using TodoApi.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Security.Claims;
using TodoApi.Authentication;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Hl7.Fhir.Utility;

namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IConfiguration _configuration;



        public AuthenticationController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            _configuration = configuration;

        }


        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            var userExit = await userManager.FindByNameAsync(model.UserName);
            if (userExit != null)
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", massage = "User Already Exits" });
            ApplicationUser user = new ApplicationUser()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.UserName

            };
            var result = await userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", massage = "Account Creation Failed" });
            }

            return Ok(new Response { status = "Success", massage = "Account create successfully" });

        }

        //------------------------------------------------------------------------------>>>>
        //User Login;
        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            var user = await userManager.FindByNameAsync(model.UserName);
            if (user != null && await userManager.CheckPasswordAsync(user, model.Password))
            {
                var userRoles = await userManager.GetRolesAsync(user);
                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name,user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),

                };
                foreach (var userRole in userRoles)
                {

                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                }
                var authSinginKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));
                var token = new JwtSecurityToken(

                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSinginKey, SecurityAlgorithms.HmacSha256));

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),

                });
            }
            return Unauthorized();
        }

        //---------------------------------------------------------------------------->>>>

        [HttpPost]
        [Route("RegisterAdmin")]
        public async Task<IActionResult> RegisterAdmin([FromBody] RegisterModel model)
        {
            var userExit = await userManager.FindByNameAsync(model.UserName);
            if (userExit != null)
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", massage = "User Already Exits" });
            ApplicationUser user = new ApplicationUser()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.UserName

            };
            var result = await userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", massage = "Account Creation Failed" });
            }

            if (!await roleManager.RoleExistsAsync(userRoles.Admin))
                await roleManager.CreateAsync(new IdentityRole(userRoles.Admin));
            if (!await roleManager.RoleExistsAsync(userRoles.User))
                await roleManager.CreateAsync(new IdentityRole(userRoles.User));
            if (await roleManager.RoleExistsAsync(userRoles.Admin))
            {
                await userManager.AddToRoleAsync(user, userRoles.Admin);
            }

            return Ok(new Response { status = "Success", massage = "Account create successfully" });

        }
        //----------------------------------------------------------------------------------------->>>>>>

        //----------------------------------------------------------------------------->>>>>>>>
        // UserInformation
        [HttpGet]
        [Route("UserInfo")]
        public async Task<IActionResult> UserInfo([FromBody] UserProfile model)
        {
            var user = await userManager.FindByNameAsync(model.UserName);
            if (user == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", massage = "User Already Exits" });

            }
            else
            
                // user.UserName = model.UserName;

                //  user.DateOfBirth = model.DateOfBirth;
                return Ok(new { user.UserName});
            



            
        }

        // UserProfile
        [HttpPost]
        [Route("UserProfile")]
        public async Task<IActionResult> UserProfile([FromBody] UserProfile model)
        {
            var user = await userManager.FindByNameAsync(model.UserName);
            if (user == null)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new Response { status = "Error", massage = "User Already Exits" });

            }
            else
            {
                user.Address = model.Address;
               // user.FirstName = model.FirstName;
               // user.LastName = model.LastName;

            }

            var result = await userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                return Ok(new Response { status = "Success", massage = "UserProfileUpdated" });
            }
            // return result;
            return BadRequest();

        }
    }
}
