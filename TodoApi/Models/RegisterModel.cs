﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApi.Models
{
    public class RegisterModel
    {

        [Required(ErrorMessage = "User Name is required")]
        public String UserName { set; get; }

        [Required(ErrorMessage = "Email is required")]
        public String Email { set; get; }

        [Required(ErrorMessage = "Password is required")]
        public String Password { set; get; }
    }
}
