﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TodoApi.Authentication
{
    public   class UserProfile
    {

        public string UserName{ get; set; }

        public string Address{ get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
