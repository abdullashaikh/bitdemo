﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace TodoApi.Authentication
{

    public class ApplicationUser : IdentityUser
    {
        public string UserName { get; set; }
        public string Address{ get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }



    }
}
